# Remote Errors

```javascript
require('zrim-errors').remote
```

Contains remote errors

## List of errors

- RemoteError : Main error for remote error
- RemoteServiceError : The remote service is not available
- RemoteServiceError : The remote service returned error
