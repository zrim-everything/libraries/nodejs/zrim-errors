# JavaScript Errors

## Introduction

Contains a set of javascript errors.

## Base Error

See:
- [BaseError](base-error.md)
- [BaseTypeError](base-type-error.md)

## Namespaces

- [common](common-errors.md)
- [remote](remote-errors.md)
- [security](security-errors.md)
