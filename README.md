# Zrim Errors

```javascript
require('zrim-errors')
```

## Introduction

Provide new common errors for application.

The base classes provide new feature like :
- default message
- contains the cause error

See [documentation](documentation/README.md)

## Requirement

The library require ES2017.
